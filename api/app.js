var express = require('express')
var path = require('path')
var logger = require('morgan')
var mongoose = require('mongoose')
var cookieParser = require('cookie-parser')
var bodyParser = require('body-parser')
var passport = require('passport')
var index = require('./routes/index')
var users = require('./routes/users')
var items = require('./routes/shopItems')
var jobs = require('./routes/jobs')
var auth = require('./routes/auth')
var lazyLoad = require('./routes/lazyLoad')

var app = express()

// Use native Node promises
mongoose.Promise = global.Promise

// connect to MongoDB
mongoose.connect('mongodb://localhost/api')
    .then(() => console.log('connection succesful'))
    .catch((err) => console.error(err))

// view engine setup
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'ejs')

// Use the passport package in our application
app.use(passport.initialize())
app.use(passport.session())

// uncomment after placing your favicon in /public
// app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')))
app.use(logger('dev'))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: false}))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'public')))

// Makes an universal options function for the api
function options(req, res, next) {
    if (req.method === 'OPTIONS') {
        console.log('!OPTIONS')
        var headers = {}
        // IE8 does not allow domains to be specified, just the *
        // headers["Access-Control-Allow-Origin"] = req.headers.origin
        headers["Access-Control-Allow-Origin"] = "*"
        headers["Access-Control-Allow-Methods"] = "POST, GET, PUT, DELETE, OPTIONS"
        headers["Access-Control-Allow-Credentials"] = false
        headers["Access-Control-Max-Age"] = '86400' // 24 hours
        headers["Access-Control-Allow-Headers"] = "X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept"
        res.writeHead(200, headers)
        res.end()
    } else {
        next()
    }
}

app.use(options)

// Config routes
app.use('/', index)
app.use('/users', users)
app.use('/shopItems', items)
app.use('/jobs', jobs)
app.use('/auth', auth.router)
app.use('/lazyLoad', lazyLoad)

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found')
    err.status = 404
    next(err)
})

// error handler
app.use(function (err, req, res) {
    // set locals, only providing error in development
    res.locals.message = err.message
    res.locals.error = req.app.get('env') === 'development' ? err : {}

    // render the error page
    res.status(err.status || 500)
    res.render('error')
})


module.exports = app
