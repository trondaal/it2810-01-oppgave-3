// script for deleting the database

var mongoose = require('mongoose')

// Use native Node promises
mongoose.Promise = global.Promise
// connect to MongoDB
mongoose.connect('mongodb://localhost/api')
    .then(() => console.log('connection succesful'))
    .catch((err) => console.error(err))

mongoose.connection.once('connected', () => {
    mongoose.connection.db.dropDatabase(() => {
        console.log("dropped db")
        process.exit()
    })
})