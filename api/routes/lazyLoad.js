var express = require('express')
var User = require('../models/User')
var router = express.Router()

/* GET users listing using paginating for dynamic loading*/
router.get('/:offset', function (req, res, next) {
    User.find()
    User.paginate({}, {offset: req.params.offset, limit: 5}, function (err, users) {
        if (err) return next(err)
        res.json(users)
    }
    )
})


module.exports = router
