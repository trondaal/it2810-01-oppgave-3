var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var Job = require('../models/Job');


/* GET jobs listing. */
router.get('/', function (req, res, next) {
    Job.find(function (err, jobs) {
        if (err) return next(err);
        res.json(jobs);
    });
});

// Post to jobs
router.post('/', function(req, res, next) {
    Job.create(req.body, function (err, post) {
        if (err) return next(err);
        res.json(post);
    });
});

// Delete jobs
router.delete('/:id', function(req, res, next) {
    Job.findByIdAndRemove(req.params.id, req.body, function (err, post) {
        if (err) return next(err);
        res.json(post);
    });
});

module.exports = router;
