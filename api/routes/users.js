var express = require('express')
var User = require('../models/User')

var router = express.Router()

var passport = require('passport')
require('../passport')(passport)

/* GET users listing. */
router.get('/', function (req, res, next) {
    User.find(function (err, users) {
        if (err) return next(err)
        res.json(users)
    })
})

// Get user with id
router.get('/:id', function (req, res, next) {
    User.findById(req.params.id, function (err, users) {
        if (err) return next(err)
        res.json(users)
    })
})

// Post user
router.post('/', function(req, res, next) {
    User.create(req.body, function (err, post) {
        if (err) return next(err)
        res.json(post)
    })
})

// Delete user
router.delete('/:id', function(req, res, next) {
    User.findByIdAndRemove(req.params.id, req.body, function (err, post) {
        if (err) return next(err)
        res.json(post)
    })
})

// Update user
router.put('/:id', function (req, res) {
    User.findById(req.params.id, function (err, users) {
        console.log(req.body.car)
        users.car = req.body.car
        users.computer = req.body.computer
        users.office = req.body.office
        users.currentMoney = req.body.currentMoney
        users.searchHistory = req.body.searchHistory
        users.backendExp = req.body.backendExp
        users.frontendExp = req.body.frontendExp
        users.maintenanceExp = req.body.maintenanceExp
        users.totalMoney = req.body.totalMoney
        users.currentJob = req.body.currentJob
        return users.save(function (err) {
            if (!err) {
                console.log("updated")
            } else {
                console.log(err)
            }
            return res.send(users)
        })
    })
})

module.exports = router
