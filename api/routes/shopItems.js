var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var Item = require('../models/Item');


/* GET items listing. */
router.get('/', function (req, res, next) {
    Item.find(function (err, shopItems) {
        if (err) return next(err);
        res.json(shopItems);
    });
});

// Post items
router.post('/', function(req, res, next) {
    Item.create(req.body, function (err, post) {
        if (err) return next(err);
        res.json(post);
    });
});

// Delete items
router.delete('/:id', function(req, res, next) {
    Item.findByIdAndRemove(req.params.id, req.body, function (err, post) {
        if (err) return next(err);
        res.json(post);
    });
});

module.exports = router;
