var express = require('express')
var router = express.Router()
// http://devdactic.com/restful-api-user-authentication-1
// load up the user model
var User = require('../models/User')
var jwt = require('jwt-simple')
var secret = "config.secretconfig.secret"


// Function for signing up
router.post('/signup', function (req, res) {
    if (!req.body.username || !req.body.password) {
        res.sendStatus(400)
    } else {
        var newUser = new User({
            username: req.body.username,
            password: req.body.password
        })
        // save the user
        newUser.save(function (err) {
            if (err) {
                console.log(err)
                return res.sendStatus(401)
            }
            res.json({success: true, msg: 'Successful created new user.'})
        })
    }
})

// Function for authenticating
router.post('/authenticate', function (req, res) {
    User.findOne({
        username: req.body.username
    }, function (err, user) {
        if (err) throw err

        if (!user) {
            res.sendStatus(400)
        } else {
            // check if password matches
            user.verifyPassword(req.body.password, function (err, isMatch) {
                if (isMatch && !err) {
                    // if user is found and password is right create a token
                    var token = jwt.encode(user, secret)
                    // return the information including token as JSON
                    res.json({success: true, token: 'JWT ' + token, user})
                } else {
                    res.sendStatus(401)
                }
            })
        }
    })
})

const getToken = (headers) => {
    if (headers && headers.authorization) {
        var parted = headers.authorization.split(' ')
        if (parted.length === 2) {
            return parted[1]
        }
    }
    return null
}

// Check if client is authenticated
function isAuthenticated(req, res, next) {
    var token = getToken(req.headers)
    if (token) {
        try {
            var decoded = jwt.decode(token, secret)
        } catch (e) {
            return res.status(403).send({success: false, msg: 'Invalid Token.'})
        }
        User.findOne({
            username: decoded.username
        }, function (err, user) {
            if (err) throw err

            if (!user) {
                return res.status(403).send({success: false, msg: 'Authentication failed. User not found.'})
            }
            req.user = user
            next()

        })
    } else {
        return res.status(403).send({success: false, msg: 'No token provided.'})
    }
}


module.exports = {
    router,
    isAuthenticated
}
