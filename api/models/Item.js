var mongoose = require('mongoose')

// Model for item
var ItemSchema = new mongoose.Schema({
    id: Number,
    name: String,
    type: String,
    color: String,
    graphicUrl: String,
    cost: Number,
    description: String,
    workPerHour: Number
})
module.exports = mongoose.model('Item', ItemSchema)
