var mongoose = require('mongoose');

// Model for job
var JobSchema = new mongoose.Schema({
    id: Number,
    name: String,
    description: String,
    travelCost: Number,
    processingCost: Number,
    officeCost: Number,
    profit: Number,
    requiredFrontendExp: Number,
    requiredBackendExp: Number,
    requiredMaintenanceExp: Number,
    frontendExp: Number,
    backendExp: Number,
    maintenanceExp: Number
});
module.exports = mongoose.model('Job', JobSchema);
