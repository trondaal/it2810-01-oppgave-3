var bcrypt = require('bcrypt')
var mongoose = require('mongoose')
// Pagination  plugin
var mongoosePaginate = require('mongoose-paginate')
mongoosePaginate.paginate.options = {
    lean: true,
    limit: 10,
    offset: 0
}

//  Model for user
var UserSchema = new mongoose.Schema({
    id: Number,
    username: {
        type: String,
        unique: true,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    searchHistory: [],
    car: {type: Number, ref: 'Item', default: 0},
    computer: {type: Number, ref: 'Item', default: 1},
    office: {type: Number, ref: 'Item', default: 2},
    currentMoney: {type: Number, default: 0},
    backendExp: {type: Number, default: 0},
    frontendExp: {type: Number, default: 0},
    maintenanceExp: {type: Number, default: 0},
    exp: {type: Number, default: 0},
    totalMoney: {type: Number, default: 0},
    currentJob: {type: Number, ref: 'Job'}
})

// Adding pagination plugin
UserSchema.plugin(mongoosePaginate)
mongoose.model('Model', UserSchema) // Model.paginate()

UserSchema.pre('save', function (next) {
    var user = this
    if (this.isModified('password') || this.isNew) {
        bcrypt.genSalt(10, function (err, salt) {
            if (err) {
                return next(err)
            }
            bcrypt.hash(user.password, salt, function (err, hash) {
                if (err) {
                    return next(err)
                }
                user.password = hash
                next()
            })
        })
    } else {
        return next()
    }
})

// Password verification
UserSchema.methods.verifyPassword = function (password, cb) {
    bcrypt.compare(password, this.password, function (err, isMatch) {
        if (err) return cb(err)
        cb(null, isMatch)
    })
}

module.exports = mongoose.model('User', UserSchema)
