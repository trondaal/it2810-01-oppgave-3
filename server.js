/* eslint-disable */
var express = require('express');
var path = require('path');
var fs = require('fs');
var api = require('./api/app');

var app = express(); 
var port = 8000;

// Add headers for CORS and allowed headers to allow request from other servers and auth trough headers
app.use(function(req, res, next) {
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
	next();
});

// serve static files
app.use("/static", express.static(__dirname + '/build/static'));
app.use("/img", express.static(__dirname + '/build/img'));

// serve api
app.use('/api', api);

// server al other requests to build/index.html
app.get('/*', function(req, res){
	fs.readFile('build/index.html', function(err, data){
		res.writeHead(200, {'Content-Type': 'text/html'});
		res.end(data);
	});
});


// start server
app.listen(port);
console.log("Server running")
console.log("Press CTRL+C to stop server")