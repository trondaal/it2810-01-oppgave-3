import React from 'react'
import ReactDOM from 'react-dom'
import App from './App'

/**
 * Method call telling react to render our app in the div element with id root
 *
 * Where to go next: ./App
 */
ReactDOM.render(
    <App />,
    document.getElementById('root')
)
