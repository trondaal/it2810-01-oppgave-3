import React from 'react'
import Login from '../login/Login'
import SignUp from '../sign-up/SignUp'

import './FrontPage.scss'

/**
 * Component for the home page
 *
 * @returns {XML}
 * @constructor
 */
const FrontPage = () => {
    return (<div id="front-pages">
        <h2>WELCOME TO NORDIC CONSULTANT</h2>

        <br/>
        <div className="row">
            <div className="col s12 m5 offset-m1">
                <h3>Login existing user</h3>
                <Login />
            </div>

            <div className="col s12 m6">
                <h3>Register new user</h3>
                <SignUp />
            </div>
        </div>
    </div>)
}

export default FrontPage
