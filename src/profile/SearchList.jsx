import React, {PropTypes} from 'react'
import SearchListItem from './SearchListItem'

/**
 * Component for recent searches in the profile page
 *
 * @returns {XML}
 * @constructor
 */
const SearchList = ({users}) => {
    let content
    content = users.map((user, i) => {
        return <SearchListItem name={user} key={i}/>
    })
    return <div className="side-element">
        <h3>Recent user searches</h3>
        {content}
    </div>
}

SearchList.propTypes = {
    users: PropTypes.arrayOf(PropTypes.string).isRequired
}

export default SearchList
