import React, {PropTypes} from 'react'
import { connect } from 'react-redux'
import Inventory from './Inventory'
import {getHighScores} from '../high-score/HighScoreActions'
import SearchList from './SearchList'
import Loading from '../common/Loading'
import NotFound from '../common/NotFound'


import './Profile.scss'

/**
 * Component for the profile page
 *
 * @returns {XML}
 * @constructor
 */
class Profile extends React.Component {
    componentDidMount() {
        this.props.getHighScores()
    }

    render() {
        if (this.props.loading) {
            return <Loading />
        }
        const userName = (this.props.location.hash).substr(2)
        if (userName !== "") {
            const user = this.props.usersMap[userName]
            if (!user) {
                return <NotFound />
            }
            return <div id="profile">
                <h1>Profile - {user.username}</h1>
                <table>
                    <tbody>
                        <tr>
                            <td>Name</td>
                            <td>{user.username}</td>
                        </tr>
                        <tr>
                            <td>Password hash</td>
                            <td>{user.password}</td>
                        </tr>
                        <tr>
                            <td>Money</td>
                            <td>{user.totalMoney}</td>
                        </tr>
                        <tr>
                            <td>Exp</td>
                            <td>{user.exp}</td>
                        </tr>
                        <tr>
                            <td>Maintenance Exp</td>
                            <td>{user.maintenanceExp}</td>
                        </tr>
                        <tr>
                            <td>Frontend Exp</td>
                            <td>{user.frontendExp}</td>
                        </tr>
                        <tr>
                            <td>Backend Exp</td>
                            <td>{user.backendExp}</td>
                        </tr>
                        <tr>
                            <td>Current Money</td>
                            <td>{user.currentMoney}</td>
                        </tr>
                    </tbody>
                </table>
                <div className="row">
                    <div className="col s12 m9">
                        <Inventory/>
                    </div>
                    <div className="col s12 m3">
                        <SearchList users={user.searchHistory}/>
                    </div>
                </div>
            </div>
        }
        return (<div id="profile">
            <h1>Profile</h1>
            <table>
                <tbody>
                    <tr>
                        <td>Name</td>
                        <td>{this.props.user.username}</td>
                    </tr>
                    <tr>
                        <td>Password hash</td>
                        <td>{this.props.user.password}</td>
                    </tr>
                    <tr>
                        <td>Money</td>
                        <td>{this.props.user.totalMoney}</td>
                    </tr>
                    <tr>
                        <td>Exp</td>
                        <td>{this.props.user.exp}</td>
                    </tr>
                    <tr>
                        <td>Maintenance Exp</td>
                        <td>{this.props.user.maintenanceExp}</td>
                    </tr>
                    <tr>
                        <td>Frontend Exp</td>
                        <td>{this.props.user.frontendExp}</td>
                    </tr>
                    <tr>
                        <td>Backend Exp</td>
                        <td>{this.props.user.backendExp}</td>
                    </tr>
                    <tr>
                        <td>Current Money</td>
                        <td>{this.props.user.currentMoney}</td>
                    </tr>
                </tbody>
            </table>
            <div className="row">
                <div className="col s12 m9">
                    <Inventory/>
                </div>
                <div className="col s12 m3">
                    <SearchList users={this.props.user.searchHistory}/>
                </div>
            </div>
        </div>)
    }
}

Profile.propTypes = {
    getHighScores: PropTypes.func.isRequired,
    users: PropTypes.arrayOf(PropTypes.shape()).isRequired,
    usersMap: PropTypes.shape().isRequired,
    user: PropTypes.shape().isRequired,
    location: PropTypes.shape().isRequired,
    loading: PropTypes.bool.isRequired
}

const mapStateToProps = (state) => {
    return {
        users: state.highscores.scores,
        loading: state.highscores.loadingHighScores,
        usersMap: state.highscores.scores.reduce((map, user) => {
            map[user.username] = user
            return map
        }, {}),
        user: state.login.currentUser
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        getHighScores: () => {
            dispatch(getHighScores(dispatch))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Profile)
