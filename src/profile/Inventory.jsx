import React from 'react'

/**
 * Component for the inventory in the profile page
 *
 * @returns {XML}
 * @constructor
 */
const Inventory = () => {
    return (<div id="inventory">
        <h2>Inventory</h2>
        <div id="items" className="row">
            <div className="col s4">
                <img id="pc" src="/img/computer.jpg" alt="pc"/>
            </div>
            <div className="col s4">
                <img id="car" src="/img/car.jpg" alt="car"/>
            </div>
            <div className="col s4">
                <img id="office" src="/img/work.jpg" alt="office"/>
            </div>
        </div>
    </div>)
}

export default Inventory
