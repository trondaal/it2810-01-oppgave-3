import React from 'react'

/**
 * Component for the job moduke in the profile page
 *
 * @returns {XML}
 * @constructor
 */
const JobStatus = ({job}) => {
    return <div className="job-status">
        <h4>{job.description}</h4>
        <p> Time left: {job.timeLeft} seconds</p>
        <p> Salary: ${job.salary}</p>
        <p> Experience: {job.xp}xp</p>
    </div>
}

export default JobStatus
