import React, {PropTypes} from 'react'

/**
 * Component for each item in the search list in the profile page
 *
 * @returns {XML}
 * @constructor
 */
const SearchListItem = ({name}, {i} ) => {
    return <div key={i}>
        <p>{name}</p>
    </div>
}

SearchListItem.propTypes = {
    name: PropTypes.string.isRequired
}

export default SearchListItem
