import React from 'react'
/**
 * Component used to show the user the component is loading
 *
 * @returns {XML}
 * @constructor
 */
const Loading = () => {
    return (
        <img alt="Loading" src="./img/loading_icon.gif"/>
    )
}

export default Loading
