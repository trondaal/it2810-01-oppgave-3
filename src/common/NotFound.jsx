import React from 'react'

/**
 * Component used to show the user the requested page or component was not found
 *
 * @returns {XML}
 * @constructor
 */
const NotFound = () => {
    return (
        <h1>Not Found</h1>
    )
}

export default NotFound
