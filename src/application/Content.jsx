import React, {PropTypes} from 'react'

/**
 * Pastes the content of a single page to the webpage
 *
 * @param children
 * @returns {XML}
 * @constructor
 */
const Content = ({children}) => {
    return (<div id="content">
        {children}
    </div>)
}

Content.propTypes = {
    children: PropTypes.shape().isRequired
}

export default Content
