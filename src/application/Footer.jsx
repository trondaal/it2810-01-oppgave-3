import React from 'react'

/**
 *
 * @returns {XML}
 * @constructor
 * Component for the footer
 */
const Footer = () => {
    return (
        <footer id="footer">
            <div className="row">
                <div className="col s12 m3">
                    An IT2810 NTNU project <br/>
                    Group 01
                </div>
                <div className="col s12 m3 offset-m6">
                    Made in trondheim with love &#10084;
                </div>
            </div>
        </footer>
    )
}

export default Footer
