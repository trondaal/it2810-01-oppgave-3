import React from 'react'
import {Link} from 'react-router'


/**
 * Component for header
 *
 * @returns {XML}
 * @constructor
 */
const Header = () => {
    return (<header id="header">
        <div id="header-text">
            <h1>Nordic Consultant</h1>
            <p><i>the code war</i></p>
        </div>

        <div id="header-buttons">

            <Link to="/high-score">
                <img src="/img/leaderboard-icon.png" alt="leaderboard-img"/>
                <p>Leaderboard</p>
            </Link>
            <Link to="/front-page">
                <img src="/img/log-out.png" alt="log-out-img"/>
                <p>Log out</p>
            </Link>
        </div>
    </header>)
}

export default Header
