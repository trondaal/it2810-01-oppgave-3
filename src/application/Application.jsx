import React, {PropTypes} from 'react'
import Header from './Header'
import Nav from './Nav'
import Content from './Content'
import Footer from './Footer'

import './Application.scss'

import DevTools from '../dev-tools/DevTools'

/**
 * Component for webpage template. All the pages are using the same template.
 * All the pages will paste their contents in the content component.
 *
 * @param children
 * @returns {XML}
 * @constructor
 *
 * Where to go next: Header, Nav, Content, Footer
 */
const Application = ({children}) => {
    return (<div className="root">
        <Header/>
        <Nav/>
        <Content children={children}/>
        <DevTools/>
        <Footer/>
    </div>)
}

Application.propTypes = {
    children: PropTypes.shape().isRequired
}

export default Application
