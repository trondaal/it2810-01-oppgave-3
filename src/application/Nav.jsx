import React from 'react'
import {Link} from 'react-router'

/**
 * Component for the navigation bar
 *
 * @returns {XML}
 * @constructor
 */
const Nav = () => {
    return (<nav className="navbar">
        <Link id="shopButton" to="/shop">Shop</Link>
        <Link id="homeButton" to="/profile">Home</Link>
        <Link id="workButton" to="/work">Work</Link>
    </nav>)
}

export default Nav
