import React from 'react'
import HighScoreList from './HighScoreList'
import UserSearch from './user-search/UserSearch'

import './HighScore.scss'

/**
 * Component for the high-score site. Formats and presents the high-score list
 *
 * @returns {XML}
 * @constructor
 */
const HighScore = () => {
    return (<div id="highscore">
        <div className="row">
            <div className="col s12 m9">
                <h1>High Score!</h1>
            </div>
            <UserSearch/>
        </div>
        <HighScoreList/>
    </div>)
}

export default HighScore
