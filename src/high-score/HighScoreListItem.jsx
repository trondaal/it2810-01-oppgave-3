import React, {PropTypes} from 'react'
import {Link} from 'react-router'

/**
 * Generates a single item to a list
 *
 * @param user
 * @returns {XML}
 * @constructor
 */
const HighScoreListItem = ({user}) => {
    return (
        <Link to={"/profile/#/" + user.username}>
            <div className="user-container row" key={user.id}>
                <div className="col s12 m3">{user.username}</div>
                <div className="col s3 m3"> {user.exp}</div>
                <div className="col s3 m2"> {user.frontendExp}</div>
                <div className="col s3 m2"> {user.backendExp}</div>
                <div className="col s3 m2"> {user.maintenanceExp}</div>
            </div>
        </Link>
    )
}

HighScoreListItem.propTypes = {
    user: PropTypes.shape().isRequired
}

export default HighScoreListItem
