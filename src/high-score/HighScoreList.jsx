import React, {PropTypes} from 'react'
import { connect } from 'react-redux'
import HighScoreListItem from './HighScoreListItem'
import {sortHighScore, getHighScores, resetHighScore} from './HighScoreActions'
import UserSearchGraph from './UserSearchGraph'
import Loading from '../common/Loading'
import NotFound from '../common/NotFound'
import {getUsers} from './user-search/UserSearchAction'

/**
 * Combines the items into a list
 *
 * @param highscores
 * @param orderedBy
 * @param sortHighScore
 * @returns {XML}
 * @constructor
 */
class HighScoreList extends React.Component {

    componentDidMount() {
        this.props.resetHighScore()
        this.props.getHighScores()
        this.props.getUsers()
    }

    render() {
        let content
        if (this.props.loadingHighScores) {
            content = <Loading />
        } else if (this.props.loadingHighScoresFailed) {
            content = <NotFound />
        } else {
            content = this.props.highscores.map((user, i) => {
                return <HighScoreListItem key={i} user={user}/>
            })
        }

        return (<div id="high-score-list">
            <div>
                <UserSearchGraph searches={this.props.searches}/>
            </div>
            <div className="row">
                <div
                    onClick={() => { this.props.sortHighScore('username') }}
                    className={'col s12 m3 list-title' + (this.props.orderedBy === 'username' ? ' ordered-by' : '')}
                >
                    Name
                </div>
                <div
                    onClick={() => { this.props.sortHighScore('exp') }}
                    className={'col s3 m3 list-title' + (this.props.orderedBy === 'exp' ? ' ordered-by' : '')}
                >
                    EXP
                </div>
                <div
                    onClick={() => { this.props.sortHighScore('frontendExp') }}
                    className={'col s3 m2 list-title' + (this.props.orderedBy === 'frontendExp' ? ' ordered-by' : '')}
                >
                    Frontent
                </div>
                <div
                    onClick={() => { this.props.sortHighScore('backendExp') }}
                    className={'col s3 m2 list-title' + (this.props.orderedBy === 'backendExp' ? ' ordered-by' : '')}
                >
                    Backend
                </div>
                <div
                    onClick={() => { this.props.sortHighScore('maintenanceExp') }}
                    className={'col s3 m2 list-title' +
                    (this.props.orderedBy === 'maintenanceExp' ? ' ordered-by' : '')}
                >
                    Maintanance
                </div>

            </div>
            {content}
            <div id='knapp' onClick={() => { this.props.getHighScores(this.props.offset) }}>Expand list</div>
        </div>)
    }
}

HighScoreList.propTypes = {
    highscores: PropTypes.arrayOf(PropTypes.shape()).isRequired,
    orderedBy: PropTypes.string.isRequired,
    sortHighScore: PropTypes.func.isRequired,
    getHighScores: PropTypes.func.isRequired,
    getUsers: PropTypes.func.isRequired,
    resetHighScore: PropTypes.func.isRequired,
    loadingHighScores: PropTypes.bool.isRequired,
    loadingHighScoresFailed: PropTypes.bool.isRequired,
    offset: PropTypes.number.isRequired,
    searches: PropTypes.arrayOf(PropTypes.string).isRequired
}

const mapStateToProps = (state) => {
    return {
        highscores: state.highscores.scores,
        searches: state.userSearch.users.reduce((list, user) => {
            Array.prototype.push.apply(list, user.searchHistory)
            return list
        }, []),
        orderedBy: state.highscores.orderedBy,
        loadingHighScores: state.highscores.loadingHighScores,
        loadingHighScoresFailed: state.highscores.loadingHighScoresFailed,
        offset: state.highscores.offset
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        sortHighScore: (column) => {
            dispatch(sortHighScore(column))
        },
        getHighScores: (offset=0) => {
            dispatch(getHighScores(dispatch, offset))
        },
        resetHighScore: () => {
            dispatch(resetHighScore())
        },
        getUsers: () => {
            dispatch(getUsers(dispatch))
        }
    }
}

export default connect(
    mapStateToProps, mapDispatchToProps
)(HighScoreList)
