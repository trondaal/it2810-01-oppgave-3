/**
 * Defines component action types.
 * Action types is used in Action and Reducer
 * Having them defined prevents mistakes when using them
 */

export const SORT_HIGH_SCORE = 'SORT_HIGH_SCORE'

export const FETCH_HIGH_SCORES = 'FETCH_HIGH_SCORES'
export const FETCH_HIGH_SCORES_SUCCESS = 'FETCH_HIGH_SCORES_SUCCESS'
export const FETCH_HIGH_SCORES_FAIL = 'FETCH_HIGH_SCORES_FAIL'

export const RESET_HIGH_SCORES = 'RESET_HIGH_SCORES'
