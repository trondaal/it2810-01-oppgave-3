import * as ActionTypes from './HighScoreActionTypes'
import {URL} from '../App'

/**
 * sortHighScore action that order the highscore list on one column
 *
 * @param column
 * @returns {{type, column: *}}
 */
export const sortHighScore = (column) => {
    return {
        type: ActionTypes.SORT_HIGH_SCORE,
        column
    }
}

/**
 * Action for receiving HighScores
 *
 * @param users
 * @returns {{type, users: *}}
 */
export const receiveHighScores = (users) => {
    return {
        type: ActionTypes.FETCH_HIGH_SCORES_SUCCESS,
        users
    }
}

/**
 * Action for informing receiving scores failed
 *
 * @returns {{type}}
 */
const receiveHighScoresFailed = () => {
    return {
        type: ActionTypes.FETCH_HIGH_SCORES_FAIL
    }
}

/**
 * Action for getting HighScores
 *
 * @param dispatch
 * @param offset
 * @returns {{type}}
 */
export const getHighScores = (dispatch, offset) => {
    fetch(URL + '/api/lazyLoad/' + offset,
        {
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-type': 'application/json'
            }
        }
    )
        .then((response) => {
            return response.json()
        })
        .then((json) => {
            dispatch(receiveHighScores(json.docs))
            dispatch(sortHighScore('exp'))
        }).catch((e) => {
            console.log(e)
            dispatch(receiveHighScoresFailed())
        })
    return {
        type: ActionTypes.FETCH_HIGH_SCORES
    }
}

/**
 * Action for resetting the highscore state whhen loading the highscore component preventing buggs with duplicated
 * loaded users
 * @returns {{type}}
 */
export const resetHighScore = () => {
    return {
        type: ActionTypes.RESET_HIGH_SCORES
    }
}
