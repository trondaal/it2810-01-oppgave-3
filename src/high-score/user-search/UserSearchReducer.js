import * as ActionTypes from './UserSearchActionTypes'

const initialState = {
    searchString: '',
    selectedUser: null,
    users: [],
    loadingUsers: false,
    loadingUsersFailed: false
}

export default (state = initialState, action) => {
    switch (action.type) {
        case ActionTypes.FILTER_USER_NAMES:
            return {
                ...state,
                searchString: action.searchString,
                selectedUser: null
            }
        case ActionTypes.SELECT_USER:
            return {
                ...state,
                searchString: action.user,
                selectedUser: action.user
            }
        case ActionTypes.FETCH_USERS:
            return {
                ...state,
                loadingUsers: true
            }
        case ActionTypes.FETCH_USERS_SUCCESS:
            return {
                ...state,
                loadingUsers: false,
                loadingUsersFailed: false,
                users: action.users
            }
        case ActionTypes.FETCH_USERS_FAIL:
            return {
                ...state,
                loadingUsers: false,
                loadingUsersFailed: true
            }
        default:
            return state
    }
}
