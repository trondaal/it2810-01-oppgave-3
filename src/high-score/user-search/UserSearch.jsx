import React, {PropTypes} from 'react'
import {connect} from 'react-redux'
import NameSuggestion from './NameSuggestion'
import {filterUsernamesAction, selectUserAction} from './UserSearchAction'
import {updateUserSearchHistory} from '../../login/LoginActions'

import './NameSuggestion.scss'

/**
 * Component for searching for a user
 *
 * @param users
 * @param filterUsernames
 * @param searchString
 * @param selectUser
 * @param selectedUser
 * @param currentUser
 * @returns {XML}
 * @constructor
 */
const UserSearch = ({users, filterUsernames, searchString, selectUser, selectedUser, currentUser}) => {
    function updateSearch(event) {
        filterUsernames(event.target.value)
    }

    let filteredUsers = []
    if (searchString !== '') {
        filteredUsers = users.filter((username) => {
            return (username.toLowerCase().startsWith(searchString.toLowerCase()))
        })
    }

    return (<div className="col s12 m3">
        <div className="row">
            <div className="col s12">
                <input placeholder="Find user" value={searchString} onChange={updateSearch}/>
            </div>
            <div className="col s12">
                <NameSuggestion
                    filteredUsers={filteredUsers}
                    selectUser={(user) => { selectUser(currentUser, user) }}
                    selectedUser={selectedUser}
                />
            </div>
        </div>
    </div>)
}

UserSearch.propTypes = {
    users: PropTypes.arrayOf(PropTypes.string).isRequired,
    searchString: PropTypes.string.isRequired,
    selectedUser: PropTypes.string,
    currentUser: PropTypes.shape().isRequired,
    filterUsernames: PropTypes.func.isRequired,
    selectUser: PropTypes.func.isRequired
}

const mapStateToProps = (state) => {
    return {
        users: state.userSearch.users.map((user) => {
            return user.username
        }),
        searchString: state.userSearch.searchString,
        selectedUser: state.userSearch.selectedUser,
        currentUser: state.login.currentUser
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        filterUsernames: (searchString) => {
            dispatch(filterUsernamesAction(searchString))
        },
        selectUser: (currentUser, user) => {
            dispatch(selectUserAction(user))
            dispatch(updateUserSearchHistory(dispatch, currentUser ,user))
        }
    }
}

export default connect(
    mapStateToProps, mapDispatchToProps
)(UserSearch)
