import * as ActionTypes from './UserSearchActionTypes'
import {URL} from '../../App'

/**
 * Action for filtering usernames
 *
 * @param searchString
 * @returns {{type, searchString: *}}
 */
export const filterUsernamesAction = (searchString) => {
    return {
        type: ActionTypes.FILTER_USER_NAMES,
        searchString
    }
}

/**
 * Action for selecting a user
 * @param user
 * @returns {{type, user: *}}
 */
export const selectUserAction = (user) => {
    return {
        type: ActionTypes.SELECT_USER,
        user
    }
}

/**
 * Action for receiving a user
 *
 * @param users
 * @returns {{type, users: *}}
 */
export const receiveUsers = (users) => {
    return {
        type: ActionTypes.FETCH_USERS_SUCCESS,
        users
    }
}

/**
 * Action for informing that receiving users failed
 *
 * @returns {{type}}
 */
const receiveUsersFailed = () => {
    return {
        type: ActionTypes.FETCH_USERS_FAIL
    }
}

/**
 * Action for getting users
 *
 * @param dispatch
 * @returns {{type}}
 */
export const getUsers = (dispatch) => {
    fetch(URL + '/api/users/',
        {
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-type': 'application/json'
            }
        }
    ).then((response) => {
        return response.json()
    }).then((json) => {
        dispatch(receiveUsers(json))
    }).catch((e) => {
        console.log(e)
        dispatch(receiveUsersFailed())
    })
    return {
        type: ActionTypes.FETCH_USERS
    }
}
