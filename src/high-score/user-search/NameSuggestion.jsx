import React, {PropTypes} from 'react'

const NameSuggestion = ({filteredUsers, selectUser, selectedUser}) => {
    const users = filteredUsers.map((user, i) => {
        return (<div className="col s12" key={i} onClick={() => {
            selectUser(user)
        }}> {user} </div>)
    })
    if (users.length === 0 || selectedUser !== null) {
        return (<div className="row empty" id="name-suggestion-container"/>)
    }

    return (<div className="row" id="name-suggestion-container">
        {users}
    </div>)
}

NameSuggestion.propTypes = {
    filteredUsers: PropTypes.arrayOf(PropTypes.string).isRequired,
    selectedUser: PropTypes.string,
    selectUser: PropTypes.func.isRequired
}

export default NameSuggestion
