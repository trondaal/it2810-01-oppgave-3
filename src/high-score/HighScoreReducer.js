import * as ActionTypes from './HighScoreActionTypes'

/**
 * HighScore initial state
 * This would normally be loaded from our api but it is not ready yet.
 * This resuts in the bug that it appears that the list is sorted by one field when it is not. This will be fixed when
 * we have our api i place.
 * It will be done as part of the post lading pre updating state
 */
const initialState = {
    orderedBy: 'exp',
    scores: [],
    loadingHighScores: false,
    loadingHighScoresFailed: false,
    offset: 0
}

/**
 * Reducer for HighScore
 *
 * @param state
 * @param action
 * @returns {*}
 */
export default (state = initialState, action) => {
    switch (action.type) {
        case ActionTypes.SORT_HIGH_SCORE:
            return {
                ...state,
                orderedBy: action.column,
                scores: state.scores.slice(0).sort((a, b) => {
                    if (a[action.column].localeCompare) {
                        return a[action.column].localeCompare(b[action.column])
                    }
                    return b[action.column] - a[action.column]
                })
            }

        case ActionTypes.FETCH_HIGH_SCORES:
            return {
                ...state,
                offset: state.offset + 5,
                loadingHighScores: true
            }

        case ActionTypes.FETCH_HIGH_SCORES_SUCCESS:
            const scores = state.scores
            Array.prototype.push.apply(scores, action.users)
            return {
                ...state,
                orderedBy: 'xp',
                scores,
                loadingHighScores: false,
                loadingHighScoresFailed: false
            }

        case ActionTypes.FETCH_HIGH_SCORES_FAIL:
            return {
                ...state,
                loadingHighScores: false,
                loadingHighScoresFailed: true
            }

        case ActionTypes.RESET_HIGH_SCORES:
            return {
                ...state,
                scores: []
            }

        default:
            return state
    }
}
