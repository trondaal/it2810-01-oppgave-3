import {URL} from '../App'
/**
 * Module for inserting user data to database
 *
 * @param username
 * @param password
 * @returns {*}
 * @constructor
 */


function CreateUser(username, password) {
    fetch(URL + '/api/auth/signup',
        {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                username,
                password
            })
        })
}

export default CreateUser
