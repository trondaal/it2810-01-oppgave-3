import React from 'react'
import createUser from './SignUpActions'
import './signUp.scss'

/**
 * Component for the signup gui
 *
 * @returns {XML}
 * @constructor
 */

function SignUp() {
    let nameInput = ''
    let passwordInput = ''
    function onSubmit(event) {
        event.preventDefault()
        createUser(nameInput.value, passwordInput.value)
        return false
    }

    return (<div id="signUp">
        <form onSubmit={onSubmit}>
            <label>Name: <input ref={(input) => { nameInput = input } } type="text"/></label>
            <br/>
            <br/>

            <label>Password: <input ref={(input) => { passwordInput = input } } type="password"/></label>
            <br/>
            <br/>

            <input type="submit" value="Sign Up"/>
        </form>
    </div>)
}

export default SignUp
