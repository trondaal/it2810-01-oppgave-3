import React, {PropTypes} from 'react'

/**
 * @returns XML sop filter JSX
 * @constructor The const ShopFilter when called by <ShopFilter />
 * This is the filter that lets the player filter all the shop item to get the wanted item type. Like only show cars
 */
const ShopFilter = ({types, colors, colorFilter, typeFilter, updateFilter}) => {
    function updateColorFilter(e) {
        const color = e.target.value
        updateFilter(color, typeFilter)
    }

    function updateTypeFilter(e) {
        const type = e.target.value
        updateFilter(colorFilter, type)

    }

    return (<div id="shop-filter">
        <h3>Filter by</h3>
        <hr/>
        <div>
            Types:
            <form action="">
                {types.map((type, i) => {
                    return <label key={i}>
                        <input
                            type="radio"
                            name="type"
                            onClick={updateTypeFilter}
                            defaultChecked={typeFilter===type}
                            value={type}/>
                        {type.toLocaleUpperCase()}
                    </label>
                })}
                <label>
                    <input
                        type="radio"
                        name="type"
                        value=''
                        onClick={updateTypeFilter}
                        defaultChecked={!typeFilter}/>
                    ALL TYPES
                </label>
            </form>
        </div>
        <div>
            Colors:
            <form action="">
                {colors.map((color, i) => {
                    return <label key={i}>
                        <input
                            type="radio"
                            name="color"
                            onClick={updateColorFilter}
                            defaultChecked={colorFilter===color}
                            value={color}/>
                        {color.toLocaleUpperCase()}
                    </label>
                })}
                <label>
                    <input
                        type="radio"
                        name="color"
                        value=''
                        onClick={updateColorFilter}
                        defaultChecked={!colorFilter}/>
                    ALL COLORS
                </label>
            </form>
        </div>

    </div>)
}

ShopFilter.propTypes = {
    updateFilter: PropTypes.func.isRequired,
    types: PropTypes.arrayOf(PropTypes.string).isRequired,
    colors: PropTypes.arrayOf(PropTypes.string).isRequired,
    colorFilter: PropTypes.string.isRequired,
    typeFilter: PropTypes.string.isRequired
}

export default ShopFilter
