import React, {PropTypes} from 'react'
import Item from './Item'

/**
 * @returns The shop list as an JSX
 * @constructor the const ShopList when called by <ShopList />
 * This is the list where all the shop items go, filteret on the shopFilter if the player has choosen any filter.
 */
const ShopList = ({items}) => {
    return (<div id="shop-list">
        <h2>Buy items</h2>
        <div id="shop-list">
            {items.map((item) => {
                return <Item item={item} key={item._id}/>
            })}
        </div>
    </div>)
}

ShopList.propTypes = {
    items: PropTypes.arrayOf(PropTypes.shape()).isRequired
}


export default ShopList
