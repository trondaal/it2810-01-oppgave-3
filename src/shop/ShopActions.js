import * as ActionTypes from './ShopActionTypes'
import {URL} from '../App'


export const receiveShopItems = (items) => {
    return {
        type: ActionTypes.FETCH_ITEMS_SUCCESS,
        items
    }
}

const receiveShopItemsFailed = () => {
    return {
        type: ActionTypes.FETCH_ITEMS_FAIL
    }
}

export const updateFilter = (color, type) => {
    return {
        type: ActionTypes.UPDATE_FILTER,
        filter: {
            colorFilter: color,
            typeFilter: type
        }
    }
}

export const getShopItems= (dispatch) => {
    fetch(URL + '/api/shopItems')
        .then((response) => {
            return response.json()
        })
        .then((json) => {
            dispatch(receiveShopItems(json))
        }).catch(() => {
            dispatch(receiveShopItemsFailed())
        })
    return {
        type: ActionTypes.FETCH_ITEMS
    }
}
