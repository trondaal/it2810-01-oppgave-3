import React from 'react'
import OwnedItem from './OwnedItem'

/**
 * @returns returns the player owned items as JSX
 * @constructor the const shopOwned when called by <ShopOwned />
 * This is the list off all the items the plyer owns. The player can also sell the items
 */
const ShopOwned = () => {
    return (<div id="shop-owned">
        <h2>My items</h2>
        <hr/>
        <div id="shop-owned-items">
            <OwnedItem/>
            <OwnedItem/>
            <OwnedItem/>
        </div>
    </div>)
}

export default ShopOwned
