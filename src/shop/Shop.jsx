import React, {PropTypes} from 'react'
import { connect } from 'react-redux'
import ShopOwned from './ShopOwned'
import ShopFilter from './ShopFilter'
import ShopList from './ShopList'
import {getShopItems, updateFilter} from './ShopActions'

import './Shop.scss'

/**
 * @returns XML complete shop page in JSX
 * @constructor The const Shop builds the whole component by calling <Shop />
 * Component that binds all the shop components together. The shop is where the user can buy items for the game
 *
 */
class Shop extends React.Component {
    componentDidMount() {
        this.props.getShopItems()
    }

    render() {
        return (<div id="shop">
            <ShopOwned/>
            <ShopList items={this.props.items.filter((item) => {
                return (!this.props.colorFilter || item.color === this.props.colorFilter) &&
                (!this.props.typeFilter || item.type === this.props.typeFilter)
            })}/>
            <ShopFilter
                types={this.props.types}
                colors={this.props.colors}
                colorFilter={this.props.colorFilter}
                typeFilter={this.props.typeFilter}
                updateFilter={this.props.updateFilter}
            />
        </div>)
    }
}

Shop.propTypes = {
    getShopItems: PropTypes.func.isRequired,
    updateFilter: PropTypes.func.isRequired,
    items: PropTypes.arrayOf(PropTypes.shape()).isRequired,
    types: PropTypes.arrayOf(PropTypes.string).isRequired,
    colors: PropTypes.arrayOf(PropTypes.string).isRequired,
    colorFilter: PropTypes.string.isRequired,
    typeFilter: PropTypes.string.isRequired
}


const mapStateToProps = (state) => {
    return {
        items: state.shop.items,
        types: state.shop.items.reduce((types, item) => {
            if (item.type && types.indexOf(item.type) === -1) {
                types.push(item.type)
            }
            return types
        }, []),
        colors: state.shop.items.reduce((colors, item) => {
            if (item.color && colors.indexOf(item.color) === -1) {
                colors.push(item.color)
            }
            return colors
        }, []),
        colorFilter: state.shop.filter.color,
        typeFilter: state.shop.filter.type
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        getShopItems: () => {
            dispatch(getShopItems(dispatch))
        },
        updateFilter: (color, type)=> {
            dispatch(updateFilter(color, type))
        }
    }
}

export default connect(
    mapStateToProps, mapDispatchToProps
)(Shop)
