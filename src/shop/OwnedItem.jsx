import React from 'react'

const OwnedItem = () => {
    return (<div className="owned-item">
        <div className="owned-item-info">
            <img className="owned-item-img" src="./img/car.png" alt="Car"/>
            <button className="owned-item-sell-btn">Sell for 3000kr</button>
        </div>
        Item name
    </div>)
}

export default OwnedItem
