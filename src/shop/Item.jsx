import React, {PropTypes} from 'react'

/**
 * Component representing an item the user can buy
 *
 * @param item
 * @returns {XML}
 * @constructor
 */
const Item = ({item}) => {
    return <div className="item">
        <img src={item.graphicUrl} alt={item.type}/>
        <p>{item.name}</p>
        <button disabled>Buy for {item.cost}kr</button>
    </div>
}

Item.propTypes = {
    item: PropTypes.shape().isRequired
}

export default Item
