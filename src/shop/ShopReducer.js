import * as ActionTypes from './ShopActionTypes'

/**
 * HighScore initial state
 * This would normally be loaded from our api but it is not ready yet.
 * This resuts in the bug that it appears that the list is sorted by one field when it is not. This will be fixed when
 * we have our api i place.
 * It will be done as part of the post lading pre updating state
 */
const initialState = {
    items: [],
    itemsMap: {},
    loadingItems: false,
    loadingItemsFailed: false,
    filter: {
        color: "",
        type: ""
    }
}

/**
 * Reducer for HighScore
 *
 * @param state
 * @param action
 * @returns {*}
 */
export default (state = initialState, action) => {
    switch (action.type) {
        case ActionTypes.FETCH_ITEMS:
            return {
                ...state,
                loadingItems: true
            }

        case ActionTypes.FETCH_ITEMS_SUCCESS:
            return {
                ...state,
                items: action.items,
                itemsMap: action.items.reduce((map, item) => {
                    map[item._id] = item
                    return map
                }, {}),
                loadingItems: false,
                loadingItemsFailed: false
            }

        case ActionTypes.FETCH_ITEMS_FAIL:
            return {
                ...state,
                loadingItems: false,
                loadingItemsFailed: true
            }
        case ActionTypes.UPDATE_FILTER:
            return {
                ...state,
                filter: {
                    color: action.filter.colorFilter,
                    type: action.filter.typeFilter
                }
            }

        default:
            return state
    }
}
