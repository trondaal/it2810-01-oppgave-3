import React, {PropTypes} from 'react'
import { connect } from 'react-redux'

import './login.scss'

import {login, reset} from './LoginActions'


/**
 * GUI for login
 *
 * @returns {XML}
 * @constructor
 */
class Login extends React.Component {

    constructor(props) {
        super(props)

        this.onsubmit = (event) => {
            event.preventDefault()
            this.props.loginUser(this.usernameInput.value, this.passwordInput.value)
            return false
        }
    }

    componentDidMount() {
        this.props.resetLogin()
    }

    usernameInput = null
    passwordInput = null

    render() {
        const error = !this.props.loginFailed ? '' : (
            <div className="form-error">
                <p>Login failed</p>
            </div>
        )
        return (
            <div id="login">
                <form onSubmit={this.onsubmit}>
                    <label>Name: <input ref={(input) => { this.usernameInput = input }} type="text"/></label>
                    <br/>
                    <label>Password: <input ref={(input) => { this.passwordInput = input }} type="password"/></label>
                    <br/>
                    <input
                        type="submit"
                        value="Login"
                        disabled={this.props.isLogginin}
                        title={this.props.isLogginin ? 'Logging in' : 'Login'}
                    />
                    {error}
                </form>
            </div>
        )
    }
}

Login.propTypes = {
    loginUser: PropTypes.func.isRequired,
    resetLogin: PropTypes.func.isRequired,
    isLogginin: PropTypes.bool.isRequired,
    loginFailed: PropTypes.bool.isRequired
}

const mapStateToProps = (state) => {
    return {
        username: state.login.username,
        password: state.login.password,
        isLogginin: state.login.isLogginin,
        loginFailed: state.login.loginFailed
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        loginUser: (name, password) => {
            dispatch(login(dispatch, name, password))
        },
        resetLogin: () => {
            dispatch(reset())
        }
    }
}

export default connect(
    mapStateToProps, mapDispatchToProps
)(Login)
