/**
 * Defines component action types.
 * Action types is used in Action and Reducer
 * Having them defined prevents mistakes when using them
 */

export const LOGIN = 'LOGIN'
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS'
export const LOGIN_FAIL = 'LOGIN_FAIL'
export const LOGIN_RESET = 'LOGIN_RESET'
export const UPDATE_USER_HISTORY = 'UPDATE_USER_HISTORY'
