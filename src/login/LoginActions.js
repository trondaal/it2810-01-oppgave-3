import * as ActionTypes from './LoginActionTypes'
import {replace} from 'react-router-redux'
import {URL} from '../App'

/**
 * Action setting user logged in credentials
 *
 * @param token
 * @param user
 * @returns {{type, currentUser: *}}
 */
export const loginSucceeded = ({token, user}) => {
    localStorage.setItem('token', token)
    localStorage.setItem('currentUser', JSON.stringify(user))
    return {
        type: ActionTypes.LOGIN_SUCCESS,
        currentUser: user
    }
}

/**
 * Action informing login failed
 *
 * @returns {{type}}
 */
const loginFailed = () => {
    return {
        type: ActionTypes.LOGIN_FAIL
    }
}

/**
 * Action for resetting loginform when loginform is reloaded
 *
 * @returns {{type}}
 */
export const reset = () => {
    return {
        type: ActionTypes.LOGIN_RESET
    }
}

/**
 * Action for logging inn a user
 *
 * @param dispatch
 * @param username
 * @param password
 * @returns {{type}}
 */
export const login = (dispatch, username, password) => {
    fetch(URL + '/api/auth/authenticate',
        {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                username,
                password
            })
        }
    ).then((response) => {
        return response.json()
    }).then((json) => {
        dispatch(loginSucceeded(json))
        dispatch(replace({pathname: '/profile', state: {}}))
        // do something with the session
    }).catch((e) => {
        console.log(e)
        dispatch(loginFailed())
    })
    return {
        type: ActionTypes.LOGIN
    }
}

/**
 * Action for updating current users search history
 *
 * @param dispatch
 * @param currentUser
 * @param user
 * @returns {{type}}
 */
export const updateUserSearchHistory = (dispatch, currentUser, user) => {
    currentUser.searchHistory.push(user)
    localStorage.setItem("currentUser", JSON.stringify(currentUser))
    fetch(URL + '/api/users/' + currentUser._id,
        {
            method: 'PUT',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({...currentUser})
        }
    ).catch((e) => {
        console.log(e)
    })
    return {
        type: ActionTypes.UPDATE_USER_HISTORY
    }
}
