import * as ActionTypes from './LoginActionTypes'

/**
 * HighScore initial state
 * This would normally be loaded from our api but it is not ready yet.
 * This resuts in the bug that it appears that the list is sorted by one field when it is not. This will be fixed when
 * we have our api i place.
 * It will be done as part of the post lading pre updating state
 */
const initialState = {
    isLogginin: false,
    loginFailed: false,
    currentUser: JSON.parse(localStorage.getItem('currentUser'))
}

/**
 * Reducer for HighScore
 *
 * @param state
 * @param action
 * @returns {*}
 */
export default (state = initialState, action) => {
    switch (action.type) {
        case ActionTypes.LOGIN:
            return {
                ...state,
                isLogginin: true
            }

        case ActionTypes.LOGIN_SUCCESS:
            return {
                ...state,
                isLogginin: false,
                loginFailed: false,
                currentUser: action.currentUser
            }

        case ActionTypes.LOGIN_FAIL:
            return {
                ...state,
                isLogginin: false,
                loginFailed: true
            }
        case ActionTypes.LOGIN_RESET:
            return {
                ...state,
                isLogginin: false,
                loginFailed: false
            }

        default:
            return state
    }
}
