/**
 * Defines component action types.
 * Action types is used in Action and Reducer
 * Having them defined prevents mistakes when using them
 */

export const SORT_WORK = 'SORT_WORK'
export const FETCH_WORK = 'FETCH_WORK'
export const FETCH_WORK_SUCCESS = 'FETCH_WORK_SUCCESS'
export const FETCH_WORK_FAIL = 'FETCH_WORK_FAIL'
