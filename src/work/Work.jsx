import React from 'react'
//import WorkingList from './WorkingList'
import './Work.scss'
/**
 * @returns XML complete work page in JSX
 * @constructor The const Work builds the whole component by calling <Work />
 * Component that binds all the Work components together. Here the player can start jobs
 *
 */
const Work = () => {
    return (<div id="work">
        <h1>Not implemented</h1>
        <h2>We did not have time to implement this feature but it would feel odd not to have the tab for it.</h2>
    </div>)
}

export default Work
