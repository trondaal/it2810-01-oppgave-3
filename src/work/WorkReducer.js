import * as ActionTypes from './WorkActionTypes'

/**
 * Works initial state
 */
const initialState = {
    works: [],
    loadingWork: false,
    loadingWorkFailed: false
}

/**
 * Reducer for Work
 *
 * @param state
 * @param action
 * @returns {*}
 */
export default (state = initialState, action) => {
    switch (action.type) {
        case ActionTypes.SORT_WORK:
            return {
                ...state,
                works: state.scores.slice(0).sort((a, b) => {
                    if (a[action.column].localeCompare) {
                        return a[action.column].localeCompare(b[action.column])
                    }
                    return b[action.column] - a[action.column]
                })
            }

        case ActionTypes.FETCH_WORK:
            return {
                ...state,
                loadingWOrk: true
            }

        case ActionTypes.FETCH_WORK_SUCCESS:
            return {
                ...state,
                works: action.work,
                loadingWork: false,
                loadingWorkFailed: false
            }

        case ActionTypes.FETCH_WORK_FAIL:
            return {
                ...state,
                loadingWork: false,
                loadingWorkFailed: true
            }

        default:
            return state
    }
}
