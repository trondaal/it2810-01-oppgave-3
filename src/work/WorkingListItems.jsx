import React from 'react'

// Eksempel på exp, bytt ut med noe fra user staten senere
let requiredForJob = true
const frontEndExp = 100
// const backEndExp = 50;
// const maintenanceExp = 0;

const checkRequired = (required, actual) => {
    if (required > actual) {
        requiredForJob = false
        return <span className="notEnoughExp">{ required }</span>
    }
    return <span>{required}</span>

}

const buttonLoad = () => {
    if (!requiredForJob) {
        return <button>Take job</button>
    }
    return <button className="redButton">Not qualified</button>
}

/**
 * Generates a item for the work list
 * work is a json object that contains the info about the user
 *
 * @param work
 * @returns {XML}
 * @constructor
 */
const WorkingListItems = ({work}) => {
    return (
        <div className="work-container">
            { work.name }<br/>
            { work.description } {buttonLoad()}<br/>
            ${ work.profit} <br/>
            <b>Requied exp</b> Front End:
            { checkRequired(work.requiredFrontendExp, frontEndExp)} / Back End: {work.requiredBackendExp}
            / Maintance: {work.requiredMaintenanceExp} <br/>
            <b>Earned exp</b> Front End: {work.maintenanceExp}
            / Back End: {work.backendExp} / Maintance: {work.maintenanceExp} <br/>
        </div>
    )
}

export default WorkingListItems
