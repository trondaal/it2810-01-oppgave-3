import * as ActionTypes from './WorkActionTypes'

// Remove when we have api call
const exampleJson = [{
    id: 849574,
    name: 'React App',
    description: 'Create shady app',
    profit: 2000,
    requiredFrontendExp: 200,
    requiredBackendExp: 10,
    requiredMaintenanceExp: 0,
    frontendExp: 50,
    backendExp: 10,
    maintenanceExp: 0
},
    {
        id: 6543,
        name: 'Make database',
        description: 'Create a nosql mongodb for a buyer',
        profit: 2500,
        requiredFrontendExp: 0,
        requiredBackendExp: 200,
        requiredMaintenanceExp: 50,
        frontendExp: 0,
        backendExp: 70,
        maintenanceExp: 10
    },
    {
        id: 324,
        name: 'Host a server',
        description: 'Let others use your server for websites',
        profit: 1500,
        requiredFrontendExp: 0,
        requiredBackendExp: 0,
        requiredMaintenanceExp: 150,
        frontendExp: 0,
        backendExp: 0,
        maintenanceExp: 100
    }]


export const receiveWork = (work) => {
    return {
        type: ActionTypes.FETCH_WORK_SUCCESS,
        work
    }
}

// TODO Fix when we have api call
export const getWork = (dispatch) => {
    setTimeout(() => { dispatch(receiveWork(exampleJson)) }, 200)
    return {
        type: ActionTypes.FETCH_WORK
    }
}
