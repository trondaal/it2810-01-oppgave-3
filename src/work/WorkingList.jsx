import React from 'react'
import { connect } from 'react-redux'
import WorkingListItems from './WorkingListItems'
import {getWork} from './WorkActions'
/*
 import Loading from '../common/Loading'
 import NotFound from '../common/NotFound'
 */
/**
 * @returns The work list as an JSX
 * @constructor the const WorkingList when called by <WorkingList />
 * This is the list where all jobs go. It can be filtered by WorkFilter.
 */
class WorkingList extends React.Component {

    componentDidMount() {
        this.props.getWork()
    }

    render() {
        const ItemsList = this.props.works.map((work, key) => {
            return <WorkingListItems key={key} work={work}/>
        })
        return (
            <div>
                {ItemsList}
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        works: state.work.works,
        loadingWork: state.work.loadingWork,
        loadingWorkFailed: state.work.loadingWorkFailed
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        getWork: () => {
            dispatch(getWork(dispatch))
        }
    }
}

export default connect(
    mapStateToProps, mapDispatchToProps
)(WorkingList)

