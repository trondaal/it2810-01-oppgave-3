import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux'

import highScoreReducer from './high-score/HighScoreReducer'
import filterUsersReducer from './high-score/user-search/UserSearchReducer'
import loginReducer from './login/LoginReducer'
import workReducer from './work/WorkReducer'
import shopReducer from './shop/ShopReducer'

/**
 * The combined reducer of the projects reducers
 *
 * Moved out of App.js to make cleaner
 *
 * Where to go next: back to: ./App
 */
const rootReducer = combineReducers({
    routing: routerReducer,
    highscores: highScoreReducer,
    userSearch: filterUsersReducer,
    work: workReducer,
    login: loginReducer,
    shop: shopReducer
})

export default rootReducer
