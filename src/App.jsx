import React from 'react'
import { createStore, applyMiddleware } from 'redux'
import { IndexRoute, Router, Route, browserHistory } from 'react-router'
import { Provider } from 'react-redux'
import { routerMiddleware, syncHistoryWithStore } from 'react-router-redux'

/**
 * scss imports
 * scss is in dev compiled and placed into style tags in public/index.html
 */
import './grid.scss'

/**
 * Component import
 *
 * Imports the projects main components
 */
import Application from './application/Application'
import DevTools from './dev-tools/DevTools'
import HighScore from './high-score/HighScore'
import Profile from './profile/Profile'
import Shop from './shop/Shop'
import Work from './work/Work'
import FrontPage from './front-page/FrontPage'

/**
 * import reducers
 *
 * Imports a rootReducer that is the combined reducer of all the projects reducers.
 * The merging of the reducers have been moved into the file ./Reducers.js to make this file cleaner
 *
 * Where to go next: ./Reducers.js
 */
import rootReducer from './Reducers'


/**
 * Creates the apps store
 *
 * Creates the apps store with the apps combined reducers, a empty initial state, as each reducer provides it's own
 * initial state if necessary, and the DevTools enhancer
 */
const middleware = routerMiddleware(browserHistory)
const store = createStore(
    rootReducer,
    DevTools.instrument(),
    applyMiddleware(middleware)
)

/**
 * Creates the apps history
 *
 * Connects the app to the browser history so that clicking on links modifies the url and the back and forward actions
 * works
 */
const history = syncHistoryWithStore(browserHistory, store)

function requireAuth(nextState, replace) {
    /* !auth.loggedIn()*/
    if(!localStorage.getItem('token')) {
        replace({
            pathname: '/front-page',
            state: {nextPathname: nextState.location.pathname}
        })
    }
}


/**
 * App wrap all the projects components and connects the components with the history, store and defines the apps paths
 *
 * Where to go next: ./application/Application.js
 */
const App = () => {
    return (
        <Provider store={store}>
            <Router history={history}>
                <Route path="/" component={Application}>
                    <Route path="profile" component={Profile} onEnter={requireAuth}/>
                    <Route path="profile/:id" compotent={Profile}/>
                    <Route path="high-score" component={HighScore}/>
                    <Route path="shop" component={Shop}/>
                    <Route path="work" component={Work}/>
                    <Route path="leaderboard" component={HighScore}/>
                    <Route path="front-page" component={FrontPage}/>
                    <IndexRoute component={FrontPage}/>
                </Route>
            </Router>
        </Provider>
    )
}
/**
 * const defining the url to the server.
 * On dev use:
 * http://localhost:8000
 */
export const URL = ""

export default App
