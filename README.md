# Nordic Consultant
## Documentation
### Project description
Nordic Consultant er et gruppeprosjekt i faget IT2810 - Webutvikling. Prosjektet går ut på å lage en web-basert online flerspiller spill. Spillet er inspirert fra det kjente tekstbaserte spillet Nordic Mafia. I Nordic Consultant starter hver spiller som en ny utdannet IT-student. Velg å utvikle dine ferdigheter mellom front-end, back-end og drift og konkurrerer om å bli nordens beste IT-konsulent! Prosjektet er forøvrig ikke helt ferdig, og klart til å spilles, men inneholder alle funksjoner og krav som skal være med i henhold til oppgaven.


### Krav
Her er lenken til beskrivelse og løsninger/kommentarer til prosjekts krav: https://docs.google.com/spreadsheets/d/10WCx64iyIjt7JNpZAdWBTV3nkGQ1cERMI2JffdnCC_0/edit?usp=sharing


## Getting to know the project
Begynn med src/index.js og følg etter referanser til andre filer.
React er brukt i dette prosjektet. En kort forklaring til prosjekts filene er listet under.


## Installation
Installasjonen forutsetter at man har npm, Node og MongoDB installert fra før.


For å installere avhengighetene i prosjektet kjører du følgende kommando i terminalen:
`npm install`


## Run
`npm start`

`node server.js`


Bygg


`node scripts/build.js`

`node server.js`

### Arkitektur
## Overordnet arkitektur
Systemet består av tre deler; frontend i React, server i Node, og database i MongoDB. Dermed blir den overordnede arkitekturen client server, med en intern layered architecture i backend med server og database.


Vi har brukt React med Redux som er en implementasjon av Flux mønsteret. Dette gir appen vår unidirectional data flow (UDF). Dette kan minne om MVC på flere måter, men det er ikke noen tydelig kontroller som oppdaterer viewet på samme måte som i MVC. Modellen blir indirekte oppdatert ved at en action kalles. Actions trigger oppdatering av database og endring av state som trigger oppdatering av view.


### Struktur


/src
* App.js - Denne komponenten tar seg av routingen til nettsiden.

* App.test.js - Fin som inneholder tester av App.js. Filen er av 2016.10.23 uendret fra oppsettet av appen.

* grid.scss - scss fil som samler gird layout frameworket vi bruker

* index.js - tegner appen vår til div elementet `root`

* main.scss - inneholder overordnet css

* Reducers.js - kombinerer alle prosjektet reducers til en reducer. Dette dratt ut for å holde App.js ryddig.

* variables.scss - inneholder scss variabler for heleprosjektet. Holder margins og farger konsistnte og lette å endre i hele prosjektet.

* /application - Komponent for template til nettsiden, alle sidene skal bruke en felles template, der innholdet legges inn i “content” komponentet.
    * Application.js - Koden kaller på komponentene og legger de til i rekkefølgen de skal ha.
    * Content.js - Tar inn innhold til undersiden som argument og returnerer innholdet i en div-tag
    * Footer.js - Setter av rom for Footer innhold.
    * Header.js - Komponent for overskriften på nettsiden
    * Nav.js - Komponent  som lager linker til de forskjellige undersidene.

* /common - Samling av små komponenter som brukes av flere moduler av siden

* /dev-tools - Komponent som setter opp redux devtools slik vi ønsker den


* /gridcomponents - Inneholder scss til det modifiserte grid layout frameworket

* /front-page - Komponenten for log-in & registering bruker. Denne komponenten blir lastet opp aller først i appen.
FrontPage.js - Setter inn alle komponentene som skal være i front-page (log-in & sign-up komponenter).

* /high-score.js - Komponent for high-score siden
    * HighScore.js - Formaterer og presenterer en liste for highscore
    * HighScoreList.js - Setter sammen ‘items’ til en liste
    * HighScoreListItem.js - Genererer en enkelt ‘item’ til en liste
    * HighScoreReducer.js - Lager initial state, så skal den definere hvordan handlinger påvirker state.


* /log-in - Komponent for log-in siden (vises i front-page)
    * Login.js - Presenterer log-in gui
    * LoginActions.jsx -  En list av funksjoner som håndterer forskjellig handlinger i log-in
    * LoginActionsTypes.jsx -  Definerer forskjellig handlinger i konstant-type
    * LoginReducer.jsx -  Lager initial state, så skal den definere hvordan handlinger påvirker state.


* /profile - Komponent for profilsiden til brukeren
    * Inventory.js - Returnerer html for inventory komponentet, overskrift og bilder
    * JobStatus.js
    * Profile.js - Kode som kaller på komponentene for innholdet i profilsiden, som er inventory og progress.
    * Progress.js - Returnerer html for progress komponentet, her ligger det en beskrivelse av aktiviteter som pågår
    * SearchList.js -
    * SearchListItem.js -


* /shop - Komponent for shopsiden i på nettsiden
    * Item.js - Presenterer en item
    * OwnedItem.jsx - Presenterer en item brukeren eier
    * Shop.js - Setter inn  alle komponenetene som skal være i butikken
    * ShopActions - En list av funksjoner som håndterer forskjellig handlinger i shop-page
    * ShopActionTypes - Definerer forskjellig handlinger i konstant-type
    * ShopFilter.js - Komponent for å filtrere innholdet i butikken
    * ShopList.js - Returnerer HTML for innholdet i butikken
    * ShopOwned.js - Setter tingene som brukeren eier inn på nettsiden


* /sign-up - Komponent for registere en ny bruker (vises i front-page)
    * Sign-up.js - Presenterer registering-gui og håndterer funksjonene til registering-knapp


* /work - Komponent for arbeid brukere kan ta på seg
    * Work.jsx - Ikke implementert, skal presentere arbeid brukeren kan ta på seg
    * WorkActions.js - Håndtering av forskjellige arbeids-relaterte handlinger
    * WorkActionTypes.js - Definerer forskjellig handlinger i konstant-type
    * WorkingList.jsx - Returnerer en liste med alle arbeidsoppgaver brukere kan ta
    * WorkingListItems.jsx - Lager objekter for arbeidsoppgaver
    * WorkReducer.js - Returnering av notifikasjoner ved endring av state


* /api
    * app.js - Denne kompononenten tar av seg routingen i databasen
    * deletedb.js - Komponent for å slette databasen
    * Passport.js - Komponent for autentisering av brukere
    * /routes
        * auth.js - Komponent for å logge inn og ut
        * index.js - Komponent for å hente hjemmesiden
        * jobs.js - Komponent for operasjoner mot job tabellen
        * lazyLoad.js - Komponent for paginering av data (dynamisk lasting)
        * shopIte-js - Komponent for operasjoner mot item tabellen
        * Users.js - Komponent for operasjoner mot user tabellen
    * /models
        * Item.js - skjema for gjenstander
        * Job.js - skjema for jobber
        * User.js - skjema for brukere
